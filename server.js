const express = require("express");
const app = express();
const port = process.env.PORT || 3000;
const dotenv=require("dotenv")
dotenv.config()
app.get("/add", (req, res) => {
  const numbers = req.query.numbers;
  if (!numbers) {
    return res.status(400).send({ error: "Numbers are missing" });
  }
  const sum = numbers.split(",").reduce((a, b) => a + +b, 0);
  res.send({ result: sum });
});
app.get("/multiply", (req, res) => {
  const numbers = req.query.numbers;
  if (!numbers) {
    return res.status(400).send({ error: "Numbers are missing" });
  }
  const product = numbers.split(",").reduce((a, b) => a * b, 1);
  res.send({ result: product });
});

app.listen(port, () => {
  console.log("Calculator app listening at port"+port);
});
